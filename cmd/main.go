package main

import (
	"github.com/spf13/viper"
	"gitlab.com/golang-learn1/todo-app"
	"gitlab.com/golang-learn1/todo-app/pkg/handler"
	"gitlab.com/golang-learn1/todo-app/pkg/repository"
	"gitlab.com/golang-learn1/todo-app/pkg/service"
	"log"
)

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

func main() {

	if err := initConfig(); err != nil {
		log.Fatalf("Error initializing configs : %s", err)
	}

	repos := repository.NewRepository()
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	srv := new(todo.Server)
	if err := srv.Run("8080", handlers.InitRoutes()); err != nil {
		log.Fatalf("error occured while running http server: %s", err.Error())
	}
}
